#!/usr/bin/env python
# coding: utf-8

# # BOSTEN_HOUSE_PREDICTION

# Author ~ Saurabh Kumar
# 
# Date ~ 4-DEC-21

# In[1]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import statsmodels.api as sm
from sklearn import metrics

import warnings
warnings.filterwarnings('ignore', category=UserWarning, append=True)


# In[2]:


import ipywidgets as ipyw
from IPython.display import display
style = {'description_width': 'initial'}


# In[3]:


pwd


# In[4]:


import os 
path='E:\\DataScience\\MachineLearning\\Boston_house_prediction'
os.listdir(path)


# In[5]:


df =pd.read_csv('E:\\DataScience\\MachineLearning\\Boston_house_prediction\\kc_house_data.csv')


# In[6]:


df.head()


# In[7]:


df.info()


# In[8]:


df.describe()


# In[9]:


df=df.drop(["id","date"],axis=1)


# In[10]:


df=df.replace(to_replace = np.nan, value = 0) 


# In[11]:


df.notnull().count()


# In[12]:


df.shape


# In[13]:


df.isnull()


# In[14]:


corr = df.corr()
corr.shape


# In[15]:


df.corr(method='pearson')


# In[16]:


df.corr(method='kendall')


# In[17]:


df.corr(method='spearman')


# In[18]:


plt.figure(figsize=(20,20))
sns.heatmap(corr, cbar=True, square= True, fmt='.2f', annot=True, annot_kws={'size':15}, cmap='coolwarm')


# In[19]:


wig_col = ipyw.Dropdown(
                options=[col for col in df.columns.tolist()],
                description='Features VS Price ',
                disabled=False,
                layout=ipyw.Layout(width='30%', height='30px'),
                style=style)


# In[20]:


display(wig_col)
sns.scatterplot(x=df.price, y=wig_col.value, data=df)


# In[21]:


X=df.iloc[:,1:]
y=df.iloc[:,0]


# In[22]:


from sklearn.model_selection import train_test_split
X_train,X_test,y_train,y_test =train_test_split(X,y,test_size=1/3,shuffle = False,random_state=1)


# # ***SVM***

# In[23]:


import statsmodels.api as sm


# In[24]:


model =sm.OLS(y_train,X_train).fit()


# In[25]:


model.summary()


# In[26]:


y_pred=model.predict(X_test)


# In[27]:


acc_rf = metrics.r2_score(y_test, y_pred)
print('R^2:', acc_rf)
print('Adjusted R^2:',1 - (1-metrics.r2_score(y_test, y_pred))*(len(y_test)-1)/(len(y_test)-X_test.shape[1]-1))
print('MAE:',metrics.mean_absolute_error(y_test, y_pred))
print('MSE:',metrics.mean_squared_error(y_test, y_pred))
print('RMSE:',np.sqrt(metrics.mean_squared_error(y_test, y_pred)))


# In[28]:


sns.scatterplot(y_test,y_pred)


# In[29]:


sns.lineplot(y_test,y_pred)


# # ***RandomForest***

# In[30]:


from sklearn.ensemble import RandomForestRegressor
model1=RandomForestRegressor()
model1.fit(X_train,y_train)


# In[31]:


model1.predict(X_test)


# In[32]:


y_pred1 = model1.predict(X_test)


# In[33]:


sns.scatterplot(y_test,y_pred1)


# In[34]:


from sklearn import metrics


# In[35]:


acc_rf = metrics.r2_score(y_test, y_pred1)
print('R^2:', acc_rf)
print('Adjusted R^2:',1 - (1-metrics.r2_score(y_test, y_pred1))*(len(y_test)-1)/(len(y_test)-X_test.shape[1]-1))
print('MAE:',metrics.mean_absolute_error(y_test, y_pred1))
print('MSE:',metrics.mean_squared_error(y_test, y_pred1))
print('RMSE:',np.sqrt(metrics.mean_squared_error(y_test, y_pred1)))


# In[ ]:




